import Vue from 'vue'
import './plugins/vuetify'
import VueScrollTo from 'vue-scrollto'
import App from './App.vue'
import './stylus/main.styl'

Vue.config.productionTip = false

Vue.use(VueScrollTo)

new Vue({
  render: h => h(App),
}).$mount('#app')
